# Input Focus Bug

Unable to input data in the form text fields

### Installation
Type the following to install the demo.

```
npm install -g @angular/cli
npm install
```

### Running the app
Type the following to run the demo.

```
ng serve
```

### Live Demonstration

Try entering any value in the input box and it will keep losing focus, i.e. prevents the user from entering input
