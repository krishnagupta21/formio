import { Component, AfterViewInit } from '@angular/core';
import { FormioAppConfig } from 'angular-formio';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
  public form: Object = {
    "components": [
      {
          "type": "textfield",
          "key": "title",
          "inputType": "text",
          "inputFormat": "plain",
          "input": true,
          "placeholder": "",
          "prefix": "",
          "customClass": "",
          "suffix": "",
          "multiple": false,
          "protected": false,
          "persistent": true,
          "hidden": false,
          "clearOnHide": true,
          "label": "Title",
          "labelPosition": "top",
          "description": "",
          "errorLabel": "",
          "tooltip": "",
          "hideLabel": false,
          "disabled": false,
          "autofocus": false,
          "validate": {
              "minLength": "",
              "maxLength": "100",
              "pattern": "",
              "required": true,
              "customPrivate": false,
              "strictDateValidation": false,
              "custom": ""
          },
          "customIsLocked": true,
          "customIsTrackerComponent": false,
          "customIsRequired": false,
          "defaultValue": null,
          "unique": false,
          "refreshOn": "",
          "redrawOn": "",
          "tableView": true,
          "modalEdit": false,
          "tabindex": "",
          "dbIndex": false,
          "customDefaultValue": "",
          "calculateValue": "",
          "widget": {
              "type": "input"
          },
          "attributes": {},
          "validateOn": "change",
          "conditional": {
              "show": null,
              "when": null,
              "eq": ""
          },
          "overlay": {
              "style": "",
              "left": "",
              "top": "",
              "width": "",
              "height": ""
          },
          "allowCalculateOverride": false,
          "encrypted": false,
          "showCharCount": false,
          "showWordCount": false,
          "properties": {
              "availableStatuses": "[\"draft\", \"active\", \"submitted\", \"approved\", \"rejected\", \"sendInProgress\", \"successfullySent\", \"unableToSend\"]"
          },
          "allowMultipleMasks": false,
          "mask": false,
          "inputMask": "",
          "id": "e5ezwro",
          "tags": [
              "clientRequestComponent"
          ],
          "logic": [
              {
                  "name": "Logic for enabling conditionally required",
                  "trigger": {
                      "type": "javascript",
                      "javascript": "result = component.properties.availableStatuses.includes(data['status'])"
                  },
                  "actions": [
                      {
                          "name": "set required to true",
                          "type": "property",
                          "property": {
                              "label": "Required",
                              "value": "validate.required",
                              "type": "boolean"
                          },
                          "state": true
                      }
                  ]
              }
          ],
          "customConditional": "show = component.properties.availableStatuses.includes(data['status'])"
      },
      {
          "label": "Text Area",
          "tooltip": "",
          "hideLabel": false,
          "modalEdit": false,
          "customIsRequired": false,
          "permissionedProperty": false,
          "type": "textarea",
          "input": true,
          "key": "textArea",
          "placeholder": "",
          "prefix": "",
          "customClass": "",
          "suffix": "",
          "multiple": false,
          "defaultValue": null,
          "protected": false,
          "unique": false,
          "persistent": true,
          "hidden": false,
          "clearOnHide": true,
          "refreshOn": "",
          "redrawOn": "",
          "tableView": true,
          "labelPosition": "top",
          "description": "",
          "errorLabel": "",
          "tabindex": "",
          "disabled": false,
          "autofocus": false,
          "dbIndex": false,
          "customDefaultValue": "",
          "calculateValue": "",
          "widget": {
              "type": "input"
          },
          "attributes": {},
          "validateOn": "change",
          "validate": {
              "required": false,
              "custom": "",
              "customPrivate": false,
              "strictDateValidation": false,
              "minLength": "",
              "maxLength": "50000",
              "pattern": "",
              "minWords": "",
              "maxWords": "",
              "unique": false,
              "multiple": false
          },
          "conditional": {
              "show": null,
              "when": null,
              "eq": ""
          },
          "overlay": {
              "style": "",
              "left": "",
              "top": "",
              "width": "",
              "height": ""
          },
          "allowCalculateOverride": false,
          "encrypted": false,
          "showCharCount": false,
          "showWordCount": false,
          "properties": {
              "availableStatuses": "[\"draft\", \"active\", \"submitted\", \"approved\", \"rejected\", \"sendInProgress\", \"successfullySent\", \"unableToSend\"]"
          },
          "allowMultipleMasks": false,
          "mask": false,
          "inputType": "text",
          "inputFormat": "html",
          "inputMask": "",
          "rows": 3,
          "wysiwyg": false,
          "editor": "",
          "id": "e82cshs",
          "tags": [
              "clientRequestComponent"
          ],
          "logic": [
              {
                  "name": "Disable Logic",
                  "trigger": {
                      "type": "javascript",
                      "javascript": "result = data['readOnly'];"
                  },
                  "actions": [
                      {
                          "name": "Disable",
                          "type": "property",
                          "property": {
                              "label": "Disabled",
                              "value": "disabled",
                              "type": "boolean"
                          },
                          "state": "true"
                      }
                  ]
              },
              {
                  "name": "Logic for enabling conditionally required",
                  "trigger": {
                      "type": "javascript",
                      "javascript": "result = component.customIsRequired && component.properties.availableStatuses.includes(data['status'])"
                  },
                  "actions": [
                      {
                          "name": "set required to true",
                          "type": "property",
                          "property": {
                              "label": "Required",
                              "value": "validate.required",
                              "type": "boolean"
                          },
                          "state": true
                      }
                  ]
              },
              {
                  "name": "Logic for disabling conditionally required for Save action",
                  "trigger": {
                      "type": "javascript",
                      "event": "save"
                  },
                  "actions": [
                      {
                          "name": "set required to false",
                          "type": "property",
                          "property": {
                              "label": "Required",
                              "value": "validate.required",
                              "type": "boolean"
                          },
                          "state": false
                      }
                  ]
              }
          ],
          "customConditional": "show = component.properties.availableStatuses.includes(data['status'])"
      },
      {
          "input": true,
          "tableView": true,
          "label": "Status",
          "key": "status",
          "data": {
              "values": [
                  {
                      "value": "submitted",
                      "label": "Submitted"
                  },
                  {
                      "value": "approved",
                      "label": "Approved"
                  },
                  {
                      "value": "active",
                      "label": "Active"
                  },
                  {
                      "value": "draft",
                      "label": "Draft"
                  },
                  {
                      "value": "rejected",
                      "label": "Rejected"
                  },
                  {
                      "value": "sendInProgress",
                      "label": "Send In Progress"
                  },
                  {
                      "value": "successfullySent",
                      "label": "Successfully Sent"
                  },
                  {
                      "value": "unableToSend",
                      "label": "Unable To Send"
                  }
              ],
              "json": "",
              "url": "",
              "resource": "",
              "custom": ""
          },
          "valueProperty": "value",
          "defaultValue": "draft",
          "hidden": true,
          "type": "select",
          "tags": [
              "workflowComponent"
          ],
          "conditional": {
              "show": "",
              "when": null,
              "eq": ""
          },
          "properties": {},
          "placeholder": "",
          "prefix": "",
          "customClass": "",
          "suffix": "",
          "multiple": false,
          "protected": false,
          "unique": false,
          "persistent": true,
          "clearOnHide": false,
          "dataGridLabel": false,
          "labelPosition": "top",
          "labelWidth": 30,
          "labelMargin": 3,
          "description": "",
          "errorLabel": "",
          "tooltip": "",
          "hideLabel": false,
          "tabindex": "",
          "disabled": false,
          "autofocus": false,
          "dbIndex": false,
          "customDefaultValue": "",
          "calculateValue": "",
          "allowCalculateOverride": false,
          "widget": null,
          "refreshOn": "",
          "clearOnRefresh": false,
          "validateOn": "change",
          "validate": {
              "required": false,
              "custom": "",
              "customPrivate": false
          },
          "limit": 100,
          "dataSrc": "values",
          "filter": "",
          "searchEnabled": true,
          "searchField": "",
          "minSearch": 0,
          "readOnlyValue": false,
          "authenticate": false,
          "template": "<span>{{ item.label }}</span>",
          "selectFields": "",
          "searchThreshold": 0.3,
          "fuseOptions": {},
          "customOptions": {},
          "id": "emzf5t9"
      },
      {
          "title": "BrokenPreviewPanel",
          "tooltip": "",
          "collapsible": false,
          "modalEdit": false,
          "type": "panel",
          "label": "Br",
          "breadcrumb": "default",
          "input": false,
          "key": "panel",
          "tableView": false,
          "components": [
              {
                  "label": "Text Field",
                  "customIsRequired": false,
                  "permissionedProperty": false,
                  "type": "textfield",
                  "input": true,
                  "key": "textField",
                  "tableView": true,
                  "validate": {
                      "unique": false,
                      "multiple": false,
                      "required": false,
                      "custom": "",
                      "customPrivate": false,
                      "strictDateValidation": false,
                      "minLength": "",
                      "maxLength": "",
                      "pattern": ""
                  },
                  "properties": {
                      "availableStatuses": "[\"draft\", \"active\", \"submitted\", \"approved\", \"rejected\", \"sendInProgress\", \"successfullySent\", \"unableToSend\"]"
                  },
                  "tags": [
                      "clientRequestComponent"
                  ],
                  "logic": [
                      {
                          "name": "Logic for enabling conditionally required",
                          "trigger": {
                              "type": "javascript",
                              "javascript": "result = component.properties.availableStatuses.includes(data['status'])"
                          },
                          "actions": [
                              {
                                  "name": "set required to true",
                                  "type": "property",
                                  "property": {
                                      "label": "Required",
                                      "value": "validate.required",
                                      "type": "boolean"
                                  },
                                  "state": true
                              }
                          ]
                      }
                  ],
                  "customConditional": "show = component.properties.availableStatuses.includes(data['status'])",
                  "placeholder": "",
                  "prefix": "",
                  "customClass": "",
                  "suffix": "",
                  "multiple": false,
                  "defaultValue": null,
                  "protected": false,
                  "unique": false,
                  "persistent": true,
                  "hidden": false,
                  "clearOnHide": true,
                  "refreshOn": "",
                  "redrawOn": "",
                  "modalEdit": false,
                  "labelPosition": "top",
                  "description": "",
                  "errorLabel": "",
                  "tooltip": "",
                  "hideLabel": false,
                  "tabindex": "",
                  "disabled": false,
                  "autofocus": false,
                  "dbIndex": false,
                  "customDefaultValue": "",
                  "calculateValue": "",
                  "widget": {
                      "type": "input"
                  },
                  "attributes": {},
                  "validateOn": "change",
                  "conditional": {
                      "show": null,
                      "when": null,
                      "eq": ""
                  },
                  "overlay": {
                      "style": "",
                      "left": "",
                      "top": "",
                      "width": "",
                      "height": ""
                  },
                  "allowCalculateOverride": false,
                  "encrypted": false,
                  "showCharCount": false,
                  "showWordCount": false,
                  "allowMultipleMasks": false,
                  "mask": false,
                  "inputType": "text",
                  "inputFormat": "plain",
                  "inputMask": "",
                  "id": "elwfcy"
              }
          ],
          "path": "panel",
          "placeholder": "",
          "prefix": "",
          "customClass": "",
          "suffix": "",
          "multiple": false,
          "defaultValue": null,
          "protected": false,
          "unique": false,
          "persistent": false,
          "hidden": false,
          "clearOnHide": false,
          "refreshOn": "",
          "redrawOn": "",
          "labelPosition": "top",
          "description": "",
          "errorLabel": "",
          "hideLabel": false,
          "tabindex": "",
          "disabled": false,
          "autofocus": false,
          "dbIndex": false,
          "customDefaultValue": "",
          "calculateValue": "",
          "widget": null,
          "attributes": {},
          "validateOn": "change",
          "validate": {
              "required": false,
              "custom": "",
              "customPrivate": false,
              "strictDateValidation": false
          },
          "conditional": {
              "show": null,
              "when": null,
              "eq": ""
          },
          "overlay": {
              "style": "",
              "left": "",
              "top": "",
              "width": "",
              "height": ""
          },
          "allowCalculateOverride": false,
          "encrypted": false,
          "showCharCount": false,
          "showWordCount": false,
          "properties": {},
          "allowMultipleMasks": false,
          "tree": false,
          "theme": "default",
          "id": "e2apm1w"
      },
      {
          "type": "button",
          "label": "Submit",
          "key": "submit",
          "disableOnInvalid": true,
          "input": true,
          "tableView": false,
          "validate": {
              "unique": false,
              "multiple": false
          }
      }
  ]
  };
  constructor(
    public config: FormioAppConfig
  ) {}

  ngAfterViewInit() {
  }

  onButtonAction(e: any) {
    console.log(e);
  }
}
